package com.codementor.response;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.http.CacheControl;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class HeaderService {

    public ResponseEntity.BodyBuilder maxAgeZeroCache(RequestEntity userRequest, long start, ResponseEntity.BodyBuilder response) {
        RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder().withinRange('A', 'Z').build();
        return minimalHeaders(userRequest, start, response, randomStringGenerator)
            .cacheControl(CacheControl.maxAge(0, TimeUnit.SECONDS).cachePrivate().mustRevalidate());
    }

    public ResponseEntity.BodyBuilder noCache(RequestEntity userRequest, long start, ResponseEntity.BodyBuilder response) {
        RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder().withinRange('A', 'Z').build();
        return minimalHeaders(userRequest, start, response, randomStringGenerator)
            .cacheControl(CacheControl.noCache());
    }

    private ResponseEntity.BodyBuilder minimalHeaders(RequestEntity userRequest, long start, ResponseEntity.BodyBuilder response,
        RandomStringGenerator randomStringGenerator) {
        return response
            .header("X-Request-Id", userRequest.getHeaders().getOrDefault("X-Request-ID", Collections.emptyList()).toArray(new String[0]))
            .varyBy("Accept-Encoding", "Origin")
            .header("X-Content-Type-Options", "nosniff")
            .header("X-XSS-Protection", "1", "mode=block")
            .header("X-Frame-Options", "SAMEORIGIN")
            .header("X-Runtime", BigDecimal.valueOf(System.nanoTime() - start, 9).toString())
            .eTag("W/" + "\"" + randomStringGenerator.generate(15) + "\"");
    }

    public Optional<String> extractToken(RequestEntity entity) {
        return Optional.ofNullable(entity)
                       .map(RequestEntity::getHeaders)
                       .map(headers -> headers.get("x-access-token"))
                       .filter(list -> !list.isEmpty())
                       .map(list -> list.get(0));

    }

}
